<?php /* Template Name: Services Template */ get_header(); the_post(); ?>

<main role="main">
    <!-- Banner -->
    <div class="banner">
        <div class="banner-default">
            <img src="<?php echo get_template_directory_uri(); ?>/img/about/banner.png" alt="About Us">
        </div>
        <div class="bannerText">
            <h1 id="servicesTitle">Services Offered</h1>
        </div>
        <div class="banner-clear">
            <img src="<?php echo get_template_directory_uri(); ?>/img/about/bannerClear.png" alt="About Us">
        </div>
    </div>
    <!-- /Banner -->

    <!-- Top Content -->
    <section id="servicesContent">
        <div class="block01">
            <img src="<?php echo get_template_directory_uri(); ?>/img/services/balloonBoy.png" alt="Short Term Rental">
        </div>
        <div class="block02">
            <div class="summary">
                <a id="shortTermRental"></a><h2><?php the_title(); ?></h2>
                <span class="ln"></span>
                <div class="summaryContent">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </section>
    <!-- /Top Content -->

    <!-- Secondary Content -->
    <section>
        <div id="servicesSecondary">
            <div class="block02">
                <img src="<?php echo get_template_directory_uri(); ?>/img/services/tank.png" alt="Short Term Rental">
            </div>
            <div class="block01">
                <div class="summary">
                    <a id="longTermRental"></a><h2>Long Term Rental</h2>
                    <span class="ln"></span>
                    <h3>Reliable Long-Term Helium Rental Services from Airborne Helium.</h3>
                    <p>For more than three decades, Airborne Helium has provided ongoing rental services for businesses of all sizes.  We specialize in dependable deliveries, and because we never encounter shortages, you can run your business with confidence while providing the customers with the high-quality products that they expect.</p>
                    <p>Features of Airborne Helium’s long-term delivery services:</p>
                    <ul>
                        <li>No Shortages.  We work with a number of reputable distributors, so when you need a delivery, it always arrives on time -- you never need to wait during periods of high demand.</li>
                        <li>Flexible Options for All Types of Businesses.  From restaurants, grocery stores and gift shops to schools, bowling alleys and ice rinks, Airborne Helium has
                            provided services for thousands of institutions.  We provide extensive delivery and billing options for a hassle-free (and cost-effective) process.
                        </li>
                        <li>24/7 Ordering.  While our standard operating hours are from 8:30 AM to 5:30 PM CST, you can place orders at any time through our convenient voicemail system.
                            During business hours, our knowledgeable staff can provide answers, tips and realistic estimates for projects of all sizes.</li>
                        <li>A Dedicated, Family-Run Business.  We form strong relationships with all of our customers, and every member of our staff is committed to providing you with
                            excellent service.  Our goal is to deliver an excellent return on investment in every case -- no exceptions.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- /Secondary Content -->

    <!-- Third Content -->
    <section>
        <div id="servicesThird">
            <div class="block01">
                <img src="<?php echo get_template_directory_uri(); ?>/img/services/party.png" alt="Short Term Rental">
            </div>
            <div class="block02">
                <div class="summary">
                    <a id="businessEvents"></a><h2>Business Events</h2>
                    <span class="ln"></span>
                    <h3>Get Dependable Helium Rental Services for Business Events of Any Size.</h3>
                    <p>Don’t risk non-deliveries and unreliable equipment.  When you’re planning a trade show, party or other professional event, you need the peace of mind that comes from working with an experienced helium provider.</p>
                    <p>Airborne Helium can help you plan effectively, and because we never encounter shortages, you always safe, high-quality equipment with a reliable gas supply.  We have offered services for more than 30 years, and as a family-owned business, we’re proud to provide excellent customer service for clients in a variety of industries. </p>
                    <p>Our experienced staff can deliver a fast, accurate cost estimate, and we will find the best options for your event regardless of the size of your venue or other factors.  We can also explain equipment functionality and answer all of your helium-related questions, and our long-term rental options provide an ideal return on investment for ongoing projects.</p>
                    <p>When you choose Airborne Helium, you get a hassle-free process at a competitive rate.  Get started today by calling us at 1-314-302-2511 to place an order or for more information.</p>
                </div>
            </div>
        </div>
    </section>
    <!-- /Third Content -->


</main>

<?php get_footer(); ?>
