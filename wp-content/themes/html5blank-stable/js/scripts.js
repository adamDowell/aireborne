(function ($, root, undefined) {
	
	$(function () {
		
		'use strict';

		// DOM ready, take it away
        $('#menu-main-header-menu').slicknav({
            label: 'Menu'
        });
	});
	
})(jQuery, this);
