<?php /* Template Name: Contact Template */ get_header(); the_post(); ?>

<main role="main">
    <!-- Banner -->
    <div class="banner">
        <div class="banner-default">
            <img src="<?php echo get_template_directory_uri(); ?>/img/about/banner.png" alt="About Us">
        </div>
        <div class="bannerText">
            <h1 id="contactTitle">Contact Us</h1>
        </div>
        <div class="banner-clear">
            <img src="<?php echo get_template_directory_uri(); ?>/img/about/bannerClear.png" alt="About Us">
        </div>
    </div>
    <!-- /Banner -->

    <!-- Top Content -->
    <section id="contactContent">
            <div class="summary">
                <h2><?php the_title(); ?></h2>
                <span class="ln"></span>
                <div class="summaryContent">
                    <?php the_content(); ?>
                </div>
            </div>
    </section>
    <!-- /Top Content -->
</main>
</div>
<!-- /wrapper -->
    <!-- Secondary Content -->
    <section>
        <div id="contactSecondary">
            <div class="block01">
                <div id="phoneBlock">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/contact/circle_phone.png" alt="Contact">
                    <h2>Contact Us</h2>
                </div>
                <div id="contactBlock">
                    <div class="subBlock01">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/contact/letter.png" alt="Email">
                        <span>info@airbornehelium.net</span>
                    </div>
                    <div class="subBlock02">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/contact/phone.png" alt="Phone">
                        <span>(314) 302-2511</span>
                    </div>
                </div>
             </div>
            <div class="block02">
                <h3>ASK US ANYTHING</h3>
                <?php echo do_shortcode( '[contact-form-7 id="61" title="Contact form 1"]' ) ?>
            </div>
        </div>
    </section>
    <!-- /Secondary Content -->


<?php get_footer('contact'); ?>
