<?php get_header(); the_post(); ?>

	<main role="main">
		<!-- Head Banner -->
		<section id="launchBannerContent">

            <img src="<?php echo get_template_directory_uri(); ?>/img/homepage/banner.png" alt="header">
		</section>
		<!-- /Header -->

        <!-- Top Content -->
        <section id="topContent">

            <div id="block01">
                <div id="summary">
                    <div id="summaryTitle">
                        <h2><?php the_title(); ?></h2>
                    </div>
                    <span class="ln"></span>
                    <div id="summaryContent">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>

            <div id="block02">
                    <span id="tag01">HIGH QUALITY</span>
                    <span id="tag02">HELIUM</span>
                    <span id="tag03">TANKS</span>
                    <span id="tag04">NO SHORTAGES.</span>
            </div>
        </section>
        <!-- /Top Content -->

        <!-- Secondary Content -->
        <section id="serviceContent">
            <h2>HOW OUR SERVICES WORK</h2>
            <div id="sblck01" class="serviceBlock">
                <img src="<?php echo get_template_directory_uri(); ?>/img/homepage/service01.png" alt="Service 01">
                <h2>BUSINESS PARTIES</h2>
                <p>When you want to create a positive atmosphere for your next business party, trust the experts at Airborne Helium.  Our experts will work with your planners and provide access to safe, cost-effective helium tanks, ensuring a successful event at an affordable cost</p>
                <img src="<?php echo get_template_directory_uri(); ?>/img/btnMore.png" alt="Learn More">
            </div>

            <div id="sblck02" class="serviceBlock">
                <img src="<?php echo get_template_directory_uri(); ?>/img/homepage/service02.png" alt="Service 02">
                <h2>LONG TERM RENTAL</h2>
                <p>If helium is a part of your business, your deliveries need to remain consistent at regardless of sudden changes in supply and demand.  At Airborne Helium, we never encounter shortages, and our long-term rental service gives you complete control over your supply.  Flexible delivery options mean less hassle and a better return on investment.</p>
                <img src="<?php echo get_template_directory_uri(); ?>/img/btnMore.png" alt="Learn More">
            </div>

            <div id="sblck03" class="serviceBlock">
                <img src="<?php echo get_template_directory_uri(); ?>/img/homepage/service03.png" alt="Service 03">
                <h2>SHORT TERM RENTAL</h2>
                <p>Give your next party a lift.  With flexible delivery options and simple, safe tanks, Airborne Helium provides all of the resources you need for fun, vibrant arrangements.  Our affordable helium rental services are available for events of all sizes</p>
                <img src="<?php echo get_template_directory_uri(); ?>/img/btnMore.png" alt="Learn More">
            </div>

            <div id="sblck04" class="serviceBlock">
                <img src="<?php echo get_template_directory_uri(); ?>/img/homepage/service04.png" alt="Service 04">
                <h2>MORE QUESTIONS?</h2>
                <p>Airborne Helium’s friendly customer service team can provide the information you need to start planning your next event.  To get started, call 314-302-2511.  You can also place orders after business hours through our convenient voicemail system.</p>
                <img src="<?php echo get_template_directory_uri(); ?>/img/btnMore.png" alt="Learn More">
            </div>
        </section>
        <!-- /Secondary Content -->

        <!-- Clients Content -->
        <section id="clientsContent">
            <h2>OUR CLIENTS</h2>
            <img src="<?php echo get_template_directory_uri(); ?>/img/homepage/krispyKreme.png" alt="Krispy Kreme">
            <img src="<?php echo get_template_directory_uri(); ?>/img/homepage/sears.png" alt="Sears">
            <img src="<?php echo get_template_directory_uri(); ?>/img/homepage/marchOfDimes.png" alt="March of Dimes">
            <img src="<?php echo get_template_directory_uri(); ?>/img/homepage/greatClips.png" alt="Great Clips">
        </section>
        <!-- /Clients Content -->
	</main>

<?php get_footer(); ?>
