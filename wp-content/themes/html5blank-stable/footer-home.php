        <!-- Clients Content -->
        <section id="clientsContent">
            <h2>OUR CLIENTS</h2>
            <img src="<?php echo get_template_directory_uri(); ?>/img/homepage/krispyKreme.png" alt="Krispy Kreme">
            <img src="<?php echo get_template_directory_uri(); ?>/img/homepage/sears.png" alt="Sears">
            <img src="<?php echo get_template_directory_uri(); ?>/img/homepage/marchOfDimes.png" alt="March of Dimes">
            <img src="<?php echo get_template_directory_uri(); ?>/img/homepage/greatClips.png" alt="Great Clips">
        </section>
        <!-- /Clients Content -->


        <!-- footer -->
        <footer class="footer" role="contentinfo">
            <a href=""><img id="facebook" src="<?php echo get_template_directory_uri(); ?>/img/footer/facebook.png" alt="facebook"></a>
            <a href=""><img id="flickr" src="<?php echo get_template_directory_uri(); ?>/img/footer/flickr.png" alt="flickr"></a>
            <a href=""><img id="youTube" src="<?php echo get_template_directory_uri(); ?>/img/footer/youTube.png" alt="you tube"></a>
            <a href=""><img id="twitter" src="<?php echo get_template_directory_uri(); ?>/img/footer/twitter.png" alt="twitter"></a>

        </footer>
        <!-- /footer -->
        <?php wp_footer(); ?>
        <script src="<?php bloginfo('template_directory'); ?>/js/lib/jquery.slicknav.min.js" type="text/javascript"></script>


        </body>
</html>
