<?php /* Template Name: About Template */ get_header(); the_post(); ?>

<main role="main">
    <!-- Banner -->
    <div class="banner">
        <div class="banner-default">
            <img src="<?php echo get_template_directory_uri(); ?>/img/about/banner.png" alt="About Us">
        </div>
        <div class="bannerText">
            <h1>About Us</h1>
        </div>
        <div class="banner-clear">
            <img src="<?php echo get_template_directory_uri(); ?>/img/about/bannerClear.png" alt="About Us">
        </div>
    </div>
    <!-- /Banner -->

    <!-- Top Content -->
    <section id="aboutContent">
        <div class="block01">
            <img src="<?php echo get_template_directory_uri(); ?>/img/about/serviceMan.png" alt="Services">
        </div>
        <div class="block02">
            <div class="summary">
                <div class="summaryTitle">
                    <h2><?php the_title(); ?></h2>
                </div>
                <span class="ln"></span>
                <div class="summaryContent">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </section>
    <!-- /Top Content -->

    <!-- Secondary Content -->
    <section>
        <div id="aboutSecondary">
            <div class="block01">
                <div class="subBlock">
                    <h3>Features of our services include:</h3>
                    <ul>
                        <li>Friendly Service Team with Years of Combined Experience</li>
                        <li>Competitive Pricing and High-Quality Equipment</li>
                        <li>Extensive Resources for All Aspects of Helium Rental (Regulators, Balloons and More)</li>
                        <li>No Shortages or Late Deliveries</li>
                    </ul>
                </div>
            </div>
            <div class="block02">
                <div class="subBlock">
                    <p>Need to place recurring orders?  Airborne Helium is a part of your team; we’re firmly committed to providing the best set of helium options available anywhere, and regardless of the size or frequency of your rentals, we always provide the same dependable deliveries with friendly, knowledgeable customer service.</p>
                    <p>For more information, call us today at 1-800-347-4614.  Our business hours are from 8:30 AM to 5:30 PM CST, and you can place orders after business hours through our convenient voicemail system.</p>
                </div>
            </div>
        </div>
    </section>
    <!-- /Secondary Content -->

    <!-- Clients Content -->

</main>

<?php get_footer(); ?>
